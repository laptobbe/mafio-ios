//
//  MasterViewController.h
//  mafio-ios
//
//  Created by Tobias Sundstrand on 2013-05-11.
//  Copyright (c) 2013 Computertalk Sweden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController

@end
