//
//  DetailViewController.h
//  mafio-ios
//
//  Created by Tobias Sundstrand on 2013-05-11.
//  Copyright (c) 2013 Computertalk Sweden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
