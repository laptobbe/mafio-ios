//
//  main.m
//  mafio-ios
//
//  Created by Tobias Sundstrand on 2013-05-11.
//  Copyright (c) 2013 Computertalk Sweden. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
