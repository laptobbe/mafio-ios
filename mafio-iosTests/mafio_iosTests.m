//
//  mafio_iosTests.m
//  mafio-iosTests
//
//  Created by Tobias Sundstrand on 2013-05-11.
//  Copyright (c) 2013 Computertalk Sweden. All rights reserved.
//

#import "mafio_iosTests.h"

@implementation mafio_iosTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in mafio-iosTests");
}

@end
